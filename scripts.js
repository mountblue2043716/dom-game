const cards = document.querySelectorAll(".card");
const start = document.querySelector(".start-button");
let moves = 0;
let matchCount = 0;
let hasFlippedCard = false;
let clicksDisabled = false;
let firstCard, secondCard;
function flipCard() {
  if (clicksDisabled) return;
  this.classList.add("flip");
  moves += 1;
  document.querySelector(".moves").innerText = `${moves} moves`;
  if (!hasFlippedCard) {
    hasFlippedCard = true;
    firstCard = this;
    lockCard(); //to block double click on same card
  } else {
    unlockCard(); //unlock the card after other card is clicked
    hasFlippedCard = false;
    secondCard = this;
    let isMatch = firstCard.dataset.fruit === secondCard.dataset.fruit;
    isMatch ? lockCards() : unFlip();
  }
  if (matchCount === 8) {
    clearInterval(timer);
    document.querySelector(".win-message").classList.remove("hidden");
  }
}
function lockCard() {
  firstCard.removeEventListener("click", flipCard);
}
function unlockCard() {
  firstCard.addEventListener("click", flipCard);
}
function lockCards() {
  firstCard.removeEventListener("click", flipCard);
  secondCard.removeEventListener("click", flipCard);
  matchCount += 1;
}
function unFlip() {
  clicksDisabled = true; //disable clicks until unflip happens
  setTimeout(() => {
    firstCard.classList.remove("flip");
    secondCard.classList.remove("flip");
    clicksDisabled = false;
  }, 500);
}
function shuffle() {
  cards.forEach((card) => {
    let randomNumber = Math.floor(Math.random() * 16);
    card.style.order = randomNumber;
  });
}
let timerStarted;
function startTimer() {
  var startTime = new Date();
  timer = setInterval(() => {
    timerStarted = true;
    var currentTime = new Date();
    var elapsedTime = currentTime - startTime;
    document.querySelector(".time").innerText = `time: ${Math.floor(
      elapsedTime / 1000
    )} sec`;
  }, 1000);
}
function startGame() {
  shuffle();
  moves = 0;
  matchCount = 0;
  start.removeEventListener("click", startGame);
  document.querySelector(".win-message").classList.add("hidden");
  document.querySelector(".time").innerText = `time: 0 sec`;
  if (timerStarted) clearInterval(timer);
  document.querySelector(".moves").innerText = `${moves} moves`;
  cards.forEach((card) => {
    card.classList.add("flip");
  });
  setTimeout(() => {
    cards.forEach((card) => {
      card.classList.remove("flip");
    });
    cards.forEach((card) => card.addEventListener("click", flipCard));
    startTimer();
  }, 2000);
  setTimeout(() => {
    start.innerText = "Restart";
    start.addEventListener("click", startGame);
  }, 3000);
}

start.addEventListener("click", startGame);
